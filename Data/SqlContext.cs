﻿using ccu_basketball.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ccu_basketball.Data
{
    public class SqlContext : IdentityDbContext
    {
        public SqlContext(DbContextOptions<SqlContext> options) : base(options)
        {
        }
        public DbSet<StandingModel> Standing { get; set; }
        public DbSet<TeamModel> Team { get; set; }
    }
}
