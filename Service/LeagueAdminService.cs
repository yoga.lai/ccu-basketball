﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ccu_basketball.Data;
using ccu_basketball.Interface;
using ccu_basketball.Models;
using Microsoft.Extensions.Caching.Memory;

namespace ccu_basketball.Service
{
    public class LeagueAdminService:ILeagueAdminService
    {
        private readonly SqlContext _context;
        private readonly IRepo _repo;

        public LeagueAdminService(SqlContext context, IRepo repo)
        {
            _context = context;
            _repo = repo;
        }

        public void CreateNewSemesterTeam(string[] teamName, string[] area, int newSemester)
        {
            try
            {
                for (int i = 0; i < teamName.Length; i++)
                {
                    TeamModel team = new TeamModel()
                    {
                        Team_Name = teamName[i],
                        Win = 0,
                        Lose = 0,
                        Points_Difference = 0,
                        Area = area[i],
                        Semester = newSemester
                    };
                    _repo.CreateTeam(team);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("新增新學年度隊伍失敗");
                Debug.WriteLine(e.ToString());
            }
        }

        public List<TeamModel> GetLastestTeam()
        {
            return _repo.GetLastestTeam();
        }

        public List<string> GetAllTeamName(IMemoryCache memoryCache)
        {
            if (memoryCache.Get("newestTeam") == null)
            {
                int newestSemester = _repo.GetNewestSemester();
                List<string> allTeamName = _repo.GetAllTeamName(newestSemester);
                memoryCache.Set("allTeamName", allTeamName);
                return allTeamName;
            }
            return (List<string>)memoryCache.Get("newestTeam");
        }

        public void DeleteTeam(int delTeamId)
        {
            try
            {
                var delItem = _repo.GetTeamById(delTeamId);
                _repo.DeleteTeam(delItem);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Delete failed. Maybe delTeamId can't find the team");
                Debug.WriteLine(e.ToString());
            }
        }

        public void UpdateTeam(int teamId, string teamName, string area, bool playoff)
        {
            TeamModel updateInfo = new TeamModel()
            {
                Team_Id = teamId,
                Team_Name = teamName,
                Area = area,
                Playoff = playoff,
            };
            _repo.UpdateTeam(updateInfo);

        }

        public void CreateTeam(string teamName, string area, int semester)
        {
            try
            {
                TeamModel team = new TeamModel()
                {
                    Team_Name = teamName,
                    Win = 0,
                    Lose = 0,
                    Points_Difference = 0,
                    Area = area,
                    Semester = semester
                };
                _repo.CreateTeam(team);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Create team failed.");
                Debug.WriteLine(e.ToString());
            }
        }
    }
}
