﻿using System;
using System.Collections.Generic;
using System.Linq;
using ccu_basketball.Models;
using System.Diagnostics;
using ccu_basketball.Interface;

namespace ccu_basketball.Service
{
    public class StandingService : IStandingService
	{
		private readonly IRepo _repo;

		public StandingService(IRepo repo)
		{
			_repo = repo;
		}

		public void CreateStanding(StandingModel form)
		{
			TeamModel winTeam,loseTeam;
			form.Game_Date = DateTime.Now;
			form.Semester = form.Game_Date.Month >= 7
				? form.Game_Date.Year - 1911 : form.Game_Date.Year - 1911 - 1;
			_repo.CreateStanding(form);

			if (form.Team_A_Score > form.Team_B_Score)
			{
				//int pointDifference = form.Team_A_Score - form.Team_B_Score;
				winTeam = _repo.GetTeamByTeamName(form.Team_A);
				loseTeam = _repo.GetTeamByTeamName(form.Team_B);
			}
			else
			{
				winTeam = _repo.GetTeamByTeamName(form.Team_B);
				loseTeam = _repo.GetTeamByTeamName(form.Team_A);
			}
			winTeam.Win += 1;
			loseTeam.Lose += 1;
			_repo.SaveChange();
		}

		public List<TeamModel> GetTeamByArea(string area)
		{
			return _repo.GetTeamByArea(area);
		}

		public List<TeamModel> GetTeamByAreaAndSemester(string area, int semester)
		{
			return _repo.GetTeamByAreaAndSemester(area, semester);
		}

		public List<StandingModel> GetNewestStanding()
		{
			DateTime newestGameDate = _repo.GetNewestGameDate();
			return _repo.GetStandingByGameDate(newestGameDate);
		}
	}
}
