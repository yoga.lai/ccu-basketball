﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ccu_basketball.Data;
using ccu_basketball.Interface;

namespace ccu_basketball.Service
{
	public class SemesterService:ISemesterService
	{
        private readonly IRepo _repo;

		public SemesterService(IRepo repo)
        {
            _repo = repo;
        }

		public int GetNewestSemester()
        {
            return _repo.GetNewestSemester();
        }

		public List<int> GetAllSemester()
        {
            return _repo.GetAllSemester();
        }
	}
}
