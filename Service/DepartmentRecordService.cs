﻿using System.Linq;
using ccu_basketball.Data;
using ccu_basketball.Models;
using ccu_basketball.Entity;
using System.Collections.Generic;
using ccu_basketball.Interface;

namespace ccu_basketball.Service
{
	public class DepartmentRecordService:IDepartmentRecordService
	{
        private readonly IRepo _repo;
		public DepartmentRecordService(IRepo repo)
        {
            _repo = repo;
        }
		public List<HistoryGame> GetHistoryGame(string teamName)
        {
            var standings=_repo.GetStandingByTeamName(teamName);
            List<HistoryGame> historyGameList =new List<HistoryGame>();
			foreach (StandingModel s in standings)
			{
				if (s.Team_A == teamName)
				{
					HistoryGame historyGame = new HistoryGame
					{
						GameDate = s.Game_Date.ToString("yyyy-MM-dd"),
						Score = s.Team_A_Score + ":" + s.Team_B_Score,
						Opponenet = s.Team_A + " vs. " + s.Team_B
					};
					historyGameList.Add(historyGame);
				}
				else if (s.Team_B == teamName)
				{
					HistoryGame historyGame = new HistoryGame
					{
						GameDate = s.Game_Date.ToString("yyyy-MM-dd"),
						Score = s.Team_B_Score + ":" + s.Team_A_Score,
						Opponenet = s.Team_B + " vs. " + s.Team_A
					};
					historyGameList.Add(historyGame);
				}
			}
			return historyGameList;
		}		
	}
}
