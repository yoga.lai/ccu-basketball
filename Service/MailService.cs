﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ccu_basketball.Service
{
	public class MailService
	{
        private readonly string _mailAccount = "ccu.basketball71";
        private readonly string _mailPassword = "hzaluwvrmpbiuxos";
        private readonly string _mailAddress = "ccu.basketball71@gmail.com";


        public void SendFeedback(string email,string feedback)
		{
			SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new System.Net.NetworkCredential(_mailAccount, _mailPassword);
            smtp.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(_mailAddress);
            mail.To.Add(_mailAddress);
            mail.Subject = "CCU-Basketball 意見回饋";
            mail.Body = GetMailBody(email,feedback);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }

        private string GetMailBody(string email,string feedback)
        {           
            string mailBody = System.IO.File.ReadAllText("feedbackMail.html");
            mailBody = mailBody.Replace("{email}", email);
            mailBody = mailBody.Replace("{feedback}", feedback);
            return mailBody;
        }
    }
}
