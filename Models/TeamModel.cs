﻿using System.ComponentModel.DataAnnotations;

namespace ccu_basketball.Models
{
	public class TeamModel
	{
		[Key]
		public int Team_Id { get; set; }
		public string Team_Name { get; set; }
		public int Win { get; set; }
		public int Lose { get; set; }
		public int Points_Difference { get; set; }
		public string Area { get; set; }
		public int Semester { get; set; }
		public bool Playoff { get; set; }
	}
}
