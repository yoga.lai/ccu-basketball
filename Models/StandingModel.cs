﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ccu_basketball.Models
{
	public class StandingModel
	{
		[Key]
		public int Standing_Id {get;set;}
		public DateTime Game_Date {get;set;}
		public string Team_A {get;set;}
		public string Team_B { get;set;}
		public int Team_A_Score { get; set; }
		public int Team_B_Score { get; set; }
		public int Semester { get; set; }
	}
}
