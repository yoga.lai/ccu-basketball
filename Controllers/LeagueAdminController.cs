﻿using System.Collections.Generic;
using ccu_basketball.Interface;
using ccu_basketball.Models;
using ccu_basketball.Service;
using ccu_basketball.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace ccu_basketball.Controllers
{
    [Authorize]
    public class LeagueAdminController : Controller
    {
        private readonly IStandingService _standingService;
        private readonly ISemesterService _semesterService;
        private readonly ILeagueAdminService _leagueAdminService;
        private List<string> _allTeamName;

        public LeagueAdminController(IStandingService standingService,ISemesterService semesterService,ILeagueAdminService leagueAdminService,IMemoryCache memoryCache)
        {
            _standingService = standingService;
            _semesterService = semesterService;
            _leagueAdminService = leagueAdminService;
            _allTeamName=_leagueAdminService.GetAllTeamName(memoryCache);
        }

        public IActionResult Index()
        {
            ViewData["NewSemester"] = _semesterService.GetNewestSemester()+1;
            BaseViewModel model=new BaseViewModel()
            {
                AllTeamName = _allTeamName
            };
            return View(model);
        }

        public IActionResult CreateStanding()
        {
	        StandingViewModel model = new StandingViewModel
	        {
		        WestTeams = _standingService.GetTeamByArea("west"),
		        EastTeams = _standingService.GetTeamByArea("east"),
		        AllTeamName = _allTeamName
	        };
	        return View(model);
        }

        [HttpPost]
        public IActionResult CreateStanding(StandingModel form)
        {
            _standingService.CreateStanding(form);
            return RedirectToAction("Index");
        }

        public IActionResult EditTeam()
        {
            LeagueAdminViewModel model = new LeagueAdminViewModel()
            {
                AllSemester = _semesterService.GetAllSemester(),
                NewestTeams = _leagueAdminService.GetLastestTeam(),
                AllTeamName = _allTeamName
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult DeleteTeam(int delTeamId)
        {
            _leagueAdminService.DeleteTeam(delTeamId);
            return RedirectToAction("EditTeam");
        }


        [HttpPost]
        public IActionResult UpdateTeam(int updateTeamId, string teamName,string area,bool playoff)
        {
            _leagueAdminService.UpdateTeam(updateTeamId, teamName,area,playoff);
            return RedirectToAction("EditTeam");
        }

        [HttpPost]
        public IActionResult CreateTeam(string teamName, string area,int semester)
        {
            _leagueAdminService.CreateTeam(teamName, area,semester);
            return RedirectToAction("EditTeam");
        }

        public IActionResult SetNewSemester()
        {
            LeagueAdminViewModel model = new LeagueAdminViewModel()
            {
                NewestTeams = _leagueAdminService.GetLastestTeam(),
                AllTeamName = _allTeamName
            };
            return View(model); 
        }

        [HttpPost]
        public IActionResult CreateNewSemesterTeam(string[] teamName,string[] area,int newSemester)
        {
            _leagueAdminService.CreateNewSemesterTeam(teamName, area, newSemester);
            return RedirectToAction("Index");
        }
    }
}