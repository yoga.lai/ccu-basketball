﻿using ccu_basketball.Models;
using ccu_basketball.Service;
using Microsoft.AspNetCore.Mvc;
using ccu_basketball.ViewModel;
using System.Collections.Generic;
using System.Linq;
using ccu_basketball.Interface;
using Microsoft.Extensions.Caching.Memory;

namespace ccu_basketball.Controllers
{
	public class StandingController : Controller
	{
		private readonly IStandingService _standingService;
		private readonly ISemesterService _semesterService;
		private StandingViewModel _viewModel = new StandingViewModel();

		public StandingController(IStandingService standingService, ISemesterService semesterService, ILeagueAdminService leagueAdminService, IMemoryCache memoryCache)
		{
			_standingService = standingService;
			_semesterService = semesterService;
			_viewModel.AllTeamName = leagueAdminService.GetAllTeamName(memoryCache);
		}

		public IActionResult Index()
		{
			_viewModel.WestTeams = _standingService.GetTeamByArea("west");
			_viewModel.EastTeams = _standingService.GetTeamByArea("east");
			_viewModel.Semester = _semesterService.GetAllSemester();
			return View(_viewModel);
		}

		public JsonResult SelectSemester(int semester)
		{
			var westTeam = _standingService.GetTeamByAreaAndSemester("west", semester);
			var eastTeam = _standingService.GetTeamByAreaAndSemester("east", semester);
			return Json(new { westTeam, eastTeam });
		}

		public IActionResult TodayStanding()
		{
			_viewModel.NewestStanding = _standingService.GetNewestStanding();
			return View(_viewModel);
		}
	}
}
