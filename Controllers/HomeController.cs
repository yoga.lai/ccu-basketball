﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ccu_basketball.Models;
using ccu_basketball.Service;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using ccu_basketball.Interface;
using ccu_basketball.ViewModel;

namespace ccu_basketball.Controllers
{
	public class HomeController : Controller
	{

		public readonly MailService _mailService;
		public readonly ILeagueAdminService _leagueAdminService;
		private readonly IMemoryCache _memoryCache;

		public HomeController(MailService mailService, ILeagueAdminService leagueAdminService, IMemoryCache memoryCache)
		{
			_mailService = mailService;
			_memoryCache = memoryCache;
			_leagueAdminService = leagueAdminService;
		}

		public IActionResult Index()
		{
			BaseViewModel model = new BaseViewModel();
			model.AllTeamName = _leagueAdminService.GetAllTeamName(_memoryCache);
			return View(model);
		}

		public IActionResult SendFeedback(string returnUrl,string email,string feedback)
		{
			_mailService.SendFeedback(email,feedback);
			return Redirect(returnUrl);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
