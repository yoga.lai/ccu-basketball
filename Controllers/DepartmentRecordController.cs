﻿using ccu_basketball.Interface;
using ccu_basketball.ViewModel;
using ccu_basketball.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace ccu_basketball.Controllers
{
	public class DepartmentRecordController : Controller
	{
		private readonly IDepartmentRecordService _service;
		private readonly DepartmentRecordViewModel _viewModel=new DepartmentRecordViewModel();
		public DepartmentRecordController(IDepartmentRecordService service,IMemoryCache memoryCache,ILeagueAdminService leagueAdminService)
		{
			_service = service;
			_viewModel.AllTeamName=leagueAdminService.GetAllTeamName(memoryCache);
		}
		public IActionResult Index(string team)
		{
			ViewData["DepartmentTitle"] = team;
			_viewModel.HistoryGameList = _service.GetHistoryGame(team);
			return View(_viewModel);
		}
	}
}