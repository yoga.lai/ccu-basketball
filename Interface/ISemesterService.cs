﻿using System.Collections.Generic;

namespace ccu_basketball.Interface
{
    public interface ISemesterService
    {
        int GetNewestSemester();
        List<int> GetAllSemester();
    }
}
