﻿using System.Collections.Generic;
using ccu_basketball.Models;
using Microsoft.Extensions.Caching.Memory;

namespace ccu_basketball.Interface
{
    public interface ILeagueAdminService
    {
        void CreateNewSemesterTeam(string[] teamName, string[] area, int newSemester);
        List<TeamModel> GetLastestTeam();
        List<string> GetAllTeamName(IMemoryCache memoryCache);
        void DeleteTeam(int delTeamId);
        void UpdateTeam(int teamId, string teamName, string area, bool playoff);
        void CreateTeam(string teamName, string area, int semester);

    }
}
