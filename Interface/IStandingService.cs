﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ccu_basketball.Models;

namespace ccu_basketball.Interface
{
	public interface IStandingService
    {
        void CreateStanding(StandingModel form);
        List<TeamModel> GetTeamByArea(string area);
        List<TeamModel> GetTeamByAreaAndSemester(string area, int semester);
        List<StandingModel> GetNewestStanding();
    }
}
