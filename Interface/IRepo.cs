﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ccu_basketball.Models;

namespace ccu_basketball.Interface
{
	public interface IRepo
	{
		void SaveChange();
		int GetNewestSemester();
        List<int> GetAllSemester();
        List<string> GetAllTeamName(int newestSemester);
		List<TeamModel> GetLastestTeam();
		TeamModel GetTeamById(int id);
        void DeleteTeam(TeamModel team);
        void UpdateTeam(TeamModel updateInfo);
        void CreateTeam(TeamModel team);
        List<StandingModel> GetStandingByTeamName(string teamName);

        //StandingService
        List<StandingModel> GetNewestStanding();
        void CreateStanding(StandingModel newStanding);
        void UpdateTeamWinLose(string winTeam, string loseTeam);
        List<TeamModel> GetTeamByArea(string area);
        List<TeamModel> GetTeamByAreaAndSemester(string area, int semester);
        List<StandingModel> GetStandingByGameDate(DateTime gameDate);
        DateTime GetNewestGameDate();
        TeamModel GetTeamByTeamName(string teamName);
	}
}
