﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ccu_basketball.Entity;

namespace ccu_basketball.Interface
{
    public interface IDepartmentRecordService
    {
        List<HistoryGame> GetHistoryGame(string teamName);
    }
}
