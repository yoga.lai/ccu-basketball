# CCU-Basketball
CCU-Basketball提供學校籃球系隊聯盟人員，記錄各隊戰績與勝敗，使聯盟人員不再需要紙本記錄以及人工計算勝場數和敗場數

![](https://i.imgur.com/kapGi3E.jpg)

## 聯盟人員後臺
聯盟人員的管理後台，僅限聯盟人員登入，聯盟人員可在後台進行戰績的新建，隊伍的調整

![](https://i.imgur.com/c0Aat2q.png)

## 戰績統計
戰績統計方面可查看各系自己的歷史戰績與全部系所的戰績與排名
- 各系戰績

![](https://i.imgur.com/794CmZR.png)

- 本身系所歷史戰績

![](https://i.imgur.com/8N9H3jk.png)

- 當日戰績

![](https://i.imgur.com/R03bstP.jpg)


