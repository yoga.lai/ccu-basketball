﻿using System.Collections.Generic;
using ccu_basketball.Entity;

namespace ccu_basketball.ViewModel
{
	public class DepartmentRecordViewModel:BaseViewModel
	{
		public List<HistoryGame> HistoryGameList { get; set; }
	}
}
