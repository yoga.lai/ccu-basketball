﻿using System.Collections.Generic;
using ccu_basketball.Models;

namespace ccu_basketball.ViewModel
{
	public class LeagueAdminViewModel:BaseViewModel
	{
		public List<TeamModel> NewestTeams { get; set; }
		public List<int> AllSemester { get; set; }
	}
}
