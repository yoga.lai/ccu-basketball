﻿using System.Collections.Generic;
using ccu_basketball.Models;

namespace ccu_basketball.ViewModel
{
	public class StandingViewModel:BaseViewModel
	{
		public List<TeamModel> WestTeams { get; set; }
		public List<TeamModel> EastTeams { get; set; }
		public List<int> Semester { get; set; }
		public List<StandingModel> NewestStanding { get; set; }
		
	}
}
