﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ccu_basketball.Entity
{
	public class HistoryGame
	{
		public string GameDate { get; set; }
		public string Score { get; set; }
		public string Opponenet { get; set; }
	}
}
