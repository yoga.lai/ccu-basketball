﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ccu_basketball.Data;
using ccu_basketball.Interface;
using ccu_basketball.Models;

namespace ccu_basketball.Repository
{
	public class Repo : IRepo
	{
		private readonly SqlContext _context;
		public Repo(SqlContext context)
		{
			_context = context;
		}

		public void SaveChange()
		{
			_context.SaveChanges();
		}

		public int GetNewestSemester()
		{
			try
			{
				return _context.Team.Select(t => t.Semester).Max();
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
				return -1;
			}
		}

		public List<int> GetAllSemester()
		{
			return (from t in _context.Team
					orderby t.Semester descending
					select t.Semester).Distinct().ToList();
		}

		public List<string> GetAllTeamName(int newestSemester)
		{
			return _context.Team.Where(t => t.Semester == newestSemester).Select(t => t.Team_Name).ToList();
		}

		public List<TeamModel> GetLastestTeam()
		{
			int newestSemester = GetNewestSemester();
			return (from t in _context.Team
					orderby t.Area
					where t.Semester == newestSemester
					select t).ToList();
		}

        //<<<<<<< HEAD
        //        public TeamModel GetTeamById(int id)
        //        {
        //            return _context.Team.Single(t => t.Team_Id == id);
        //        }

        //        public void DeleteTeam(TeamModel team)
        //        {
        //            _context.Remove(team);
        //            _context.SaveChanges();
        //        }

        //        public void UpdateTeam(TeamModel updateInfo)
        //        {
        //            var team = GetTeamById(updateInfo.Team_Id);
        //            team.Team_Name = updateInfo.Team_Name;
        //            team.Area = updateInfo.Area;
        //            team.Playoff = updateInfo.Playoff;
        //            _context.SaveChanges();
        //        }

        //        public void CreateTeam(TeamModel team)
        //        {
        //            _context.Add(team);
        //            _context.SaveChanges();
        //        }

        //        public List<StandingModel> GetStandingByTeamName(string teamName)
        //        {
        //            int newestSemester = GetNewestSemester();
        //            return (from s in _context.Standing
        //                where (s.Team_A == teamName || s.Team_B == teamName) && s.Semester==newestSemester
        //                         select s).ToList();
        //        }

        public List<StandingModel> GetNewestStanding()
        {
            try
            {
                DateTime newestTime = _context.Standing.Select(s => s.Game_Date).Max();
                List<StandingModel> newestStanding = (from s in _context.Standing
                                                      where s.Game_Date == newestTime
                                                      select s).ToList();
                return newestStanding;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Get newestStanding failed");
                Debug.WriteLine(e.ToString());
                throw;
            }
        }

        //        public void CreateStanding(StandingModel newStanding)
        //        {
        //            _context.Add(newStanding);
        //            _context.SaveChanges();
        //        }

        public void UpdateTeamWinLose(string winTeam, string loseTeam)
        {

        }
        //    }
        //=======
        public TeamModel GetTeamById(int id)
		{
			return _context.Team.Single(t => t.Team_Id == id);
		}

		public void DeleteTeam(TeamModel team)
		{
			_context.Remove(team);
			_context.SaveChanges();
		}

		public void UpdateTeam(TeamModel updateInfo)
		{
			var team = GetTeamById(updateInfo.Team_Id);
			team.Team_Name = updateInfo.Team_Name;
			team.Area = updateInfo.Area;
			team.Playoff = updateInfo.Playoff;
			_context.SaveChanges();
		}

		public void CreateTeam(TeamModel team)
		{
			_context.Add(team);
			_context.SaveChanges();
		}

		public List<StandingModel> GetStandingByTeamName(string teamName)
		{
			int newestSemester = GetNewestSemester();
			return (from s in _context.Standing
					where (s.Team_A == teamName || s.Team_B == teamName) && s.Semester == newestSemester
					select s).ToList();
		}

		public List<TeamModel> GetTeamByArea(string area)
		{
			int teamSemester = GetNewestSemester();
			var teams = (from team in _context.Team
						 orderby team.Win descending
						 where team.Area == area && team.Semester == teamSemester
						 select team).ToList();
			return teams;
		}

		public List<TeamModel> GetTeamByAreaAndSemester(string area, int semester)
		{
			return (from team in _context.Team
					orderby team.Win descending
					where team.Area == area && team.Semester == semester
					select team).ToList();
		}

		public List<StandingModel> GetStandingByGameDate(DateTime gameDate)
		{
			try
			{
				return (from s in _context.Standing
													  where s.Game_Date == gameDate
													  select s).ToList();
			}
			catch (Exception e)
			{
				Console.WriteLine("Get newestStanding failed");
				Console.WriteLine(e.ToString());
				throw;
			}
		}

		public DateTime GetNewestGameDate()
		{
			return _context.Standing.Select(s => s.Game_Date).Max();
		}

		public TeamModel GetTeamByTeamName(string teamName)
		{
			return _context.Team.FirstOrDefault(team => team.Team_Name == teamName);
		}

		public void CreateStanding(StandingModel newStanding)
		{
			try
			{
				_context.Standing.Add(newStanding);
				_context.SaveChanges();
			}
			catch (Exception e)
			{
				Console.WriteLine("Create standing failed");
				Console.WriteLine(e.ToString());
				throw;
			}
		}
	}
//>>>>>>> 689b53f93045c3dcd52a189ae9d18916b35b0615
}
