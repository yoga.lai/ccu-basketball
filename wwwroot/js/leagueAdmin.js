﻿var CreateNewTeam = function () {
    var htmlCode = '';
    htmlCode += '<div class="form-row new-teams" >';
    htmlCode += '<div class="col">' +
                    '<input type = "text" class="form-control" placeholder = "請輸入隊伍名" name = "teamName" required>' +
                '</div >';
    htmlCode += ' <div class="col">';
    htmlCode += ' <select name="area" class="form-control">' +
                    '<option disabled selected required>請選擇分區</option>'+
                    '<option value="west">西區</option>' +
                    '<option value="east">東區</option>' +
                    '</select>' +
                ' </div>';
    htmlCode += '<div class="col delete-team">' +
                    '<i class="fa fa-trash"></i>' +
                '</div>';
    htmlCode += '</div>';

    return htmlCode;
}

//檢查表單送出時是否為空
$('#create-standing form').submit(function (e) {
    $("#create-standing form input[type!=submit]").each(function () {
        if ($(this).val() == '') {
            alert('所有欄位均需填寫');
            e.preventDefault();
            return false;
        }
    });
});

//新學期隊伍設定，刪除隊伍
$('#new-semester-section').on('click', '.delete-team', function () {
    $(this).parent('.new-teams').remove();
});

//新學期隊伍設定，新增隊伍
$('#new-semester-section .create-team.btn').click(function () {
    $('#form-title').after(CreateNewTeam());
});

//$('#setNewSemester').click(function () {
//    var newSemester=$('#setNewSemester').attr('newSemester');
//    $.post("/LeagueAdmin/CreateNewSemesterTeam", { newSemester:newSemester });
//});

//編輯隊伍，更新隊伍名稱及分區
$('#edit-team .first-update-btn').click(function () {
    var parentDom = $(this).parent('div').parent('.new-teams');
    var updateTeamId = parentDom.attr('team-id');
    var teamName = parentDom.find('input[type="text"]').val();
    var area = parentDom.find('select').val();
    var playoff = parentDom.find('input[type="checkbox"]').prop('checked');
    $('input[name="updateTeamId"]').attr('value', updateTeamId);
    $('input[name="teamName"]').attr('value', teamName);
    $('input[name="area"]').attr('value', area);
    $('input[name="playoff"]').attr('value', playoff);
});

//編輯隊伍，刪除隊伍
$('#edit-team .delete-team').click(function () {
    $('.first-delete-btn').show();
    $('.first-update-btn').hide();
    $(this).hide();
    $('#cancel-delete').show();
});

$('#cancel-delete').click(function () {
    $('.first-delete-btn').hide();
    $('.first-update-btn').show();
    $(this).hide();
    $('#edit-team .delete-team').show();
});

$('#edit-team .first-delete-btn').click(function () {
    var delTeamId = $(this).parent('div').parent('.new-teams').attr('team-id');
    $('input[name="delTeamId"]').attr('value', delTeamId);
});

//編輯隊伍，新增隊伍
$('#edit-team .create-team.btn').click(function () {
    $('#createTeamModal form input[name="semester"]').attr('value',$('#nowSemester').val());
});
