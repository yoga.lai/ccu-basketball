﻿//點擊tabs對應的隱藏顯示
$('#westOverviewTab').click(function () {
    $('#westOverview').show();
    $('.active').removeClass('active');
    $('#westOverviewTab').parent('li').addClass('active');
    $('#eastOverview').hide();
    $('#create-standing').hide();
});

$('#eastOverviewTab').click(function () {
    $('#westOverview').hide();
    $('#eastOverview').show();
    $('.active').removeClass('active');
    $('#eastOverviewTab').parent('li').addClass('active');
    $('#create-standing').hide();
});

var createTeam = function (team, overviewDom) {
    var htmlCode = '';
    for (var i = 0; i < team.length; i++) {
        htmlCode += '<tr>';
        htmlCode += '<th scope="row">' + (i + 1) + '</th>';
        htmlCode += '<td><a href="/DepartmentRecord?team=' + team[i].team_Name + '">' + team[i].team_Name + '</a ></td >';
        htmlCode += '<td>' + team[i].win + '</td>';
        htmlCode += '<td>' + team[i].lose + '</td>';
        htmlCode += '<td>' + team[i].points_Difference + '</td>';
        htmlCode += '</tr>';
    }
    overviewDom.html(htmlCode);
};

//選擇學年度的ajax
$('.semester select').change(function () {
    var url = '/Standing/SelectSemester';
    var selectSemester = $(this).val();
    var westOverview = $("#westOverview tbody");
    var eastOverview = $("#eastOverview tbody");
    $.ajax({
        url: url,
        data: { semester: selectSemester },
        beforeSend: function () {
            westOverview.html("");
            eastOverview.html("");
	        $(".loading").removeClass("hide");
        },
        success: function (data) {
	        $(".loading").addClass("hide");
            createTeam(data.westTeam, westOverview);
            createTeam(data.eastTeam, eastOverview);
        }
    });
});

